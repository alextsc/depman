# --- !Ups
CREATE TABLE GAS (
  ID          SERIAL,
  GROUP_ID    TEXT,
  ARTIFACT_ID TEXT
);

# --- !Downs

DROP TABLE GAS;