name := "depman"

version := "1.0"

lazy val `depman` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(cache,
  "org.apache.maven.indexer" % "indexer-core" % "5.1.1",
  // wagon-http pulls in httpcore 4.2.2, but we need >4.4.4
  "org.apache.maven.wagon" % "wagon-http-lightweight" % "2.3" exclude("org.apache.httpcomponents", "httpcore"),
  "org.eclipse.sisu" % "org.eclipse.sisu.plexus" % "0.3.2",
  "org.sonatype.sisu" % "sisu-guice" % "3.2.6",
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "com.typesafe.play" %% "play-slick" % "1.1.1",
  "com.typesafe.play" %% "play-slick-evolutions" % "1.1.1",
  "com.github.t3hnar" %% "scala-bcrypt" % "2.5",
  "com.h2database" % "h2" % "1.4.190",
  "org.webjars" % "bootstrap" % "4.0.0-alpha.2",
  "org.webjars" % "angularjs" % "1.5.4",
  "org.webjars" % "jquery" % "2.2.0",
  "org.webjars" % "font-awesome" % "4.5.0",
  "org.webjars" %% "webjars-play" % "2.4.0-1",
  "org.webjars.bower" % "tether" % "1.1.1",
  "org.webjars.bower" % "angular-validation-match" % "1.7.1",
  "jp.t2v" %% "play2-auth" % "0.14.1",
  "org.scalatest" %% "scalatest" % "2.2.5" % Test,
  "org.scalatestplus.play" % "scalatestplus-play_2.11" % "1.5.0" % Test,
  "org.mockito" % "mockito-all" % "1.10.19" % Test,
  play.sbt.Play.autoImport.cache
)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"