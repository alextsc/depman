package app

import org.apache.maven.index.ArtifactInfo

object ArtifactInfoFormatterImpl extends ArtifactInfoFormatter {

  override def formatSbtDependency(artifactInfo: ArtifactInfo): String =
    formatSbtDependency(artifactInfo.groupId, artifactInfo.artifactId, artifactInfo.version)

  override def formatSbtDependency(artifacts: Seq[ArtifactInfo]): String = ???

  override def formatSbtDependency(groupId: String, artifactId: String, version: String): String =
    "libraryDependencies += \"" + groupId + "\" % \"" + artifactId + "\" % \"" + version + "\""

  override def formatGradleDependency(artifactInfo: ArtifactInfo): String =
    formatGradleDependency(artifactInfo.groupId, artifactInfo.artifactId, artifactInfo.version)

  override def formatGradleDependency(artifacts: Seq[ArtifactInfo]): String =
    artifacts.map(this.formatGradleDependency).mkString("\r\n")

  override def formatGradleDependency(groupId: String, artifactId: String, version: String): String =
    s"$groupId:$artifactId:$version"

  override def formatMavenDependency(artifactInfo: ArtifactInfo): String =
    formatMavenDependency(artifactInfo.groupId, artifactInfo.artifactId, artifactInfo.version)

  override def formatMavenDependency(artifacts: Seq[ArtifactInfo]): String =
    artifacts.map(this.formatMavenDependency).mkString("\r\n")

  override def formatMavenDependency(groupId: String, artifactId: String, version: String): String =
    <dependency>
      <groupId>
        {groupId}
      </groupId>
      <artifactId>
        {artifactId}
      </artifactId>
      <version>
        {version}
      </version>
    </dependency>.toString()


}
