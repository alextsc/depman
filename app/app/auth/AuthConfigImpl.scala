package app.auth

import app.repositories.UserDao
import jp.t2v.lab.play2.auth.AuthConfig
import models.UserModel
import play.api.Logger
import play.api.mvc.{Controller, RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

sealed trait Role

object UserRole extends Role

object Administrator extends Role

trait AuthConfigImpl extends AuthConfig {
  this: Controller =>

  override type Id = Int
  override type Authority = String
  override type User = UserModel

  override def sessionTimeoutInSeconds: Int = 30 * 60

  override lazy val tokenAccessor = new RememberMeTokenAccessor(sessionTimeoutInSeconds)

  val userRepository: UserDao

  override implicit def idTag: ClassManifest[Int] = manifest[Id]

  override def resolveUser(id: Int)(implicit context: ExecutionContext): Future[Option[UserModel]] =
    userRepository.findById(id)

  override def authorize(user: UserModel, authority: String)(implicit context: ExecutionContext): Future[Boolean] =
    Future.successful(user.hasRole(authority))

  override def logoutSucceeded(request: RequestHeader)(implicit context: ExecutionContext): Future[Result] =
    Future.successful(Redirect(controllers.web.routes.AuthenticationController.login()))

  override def authenticationFailed(request: RequestHeader)(implicit context: ExecutionContext): Future[Result] =
    Future.successful(Redirect(controllers.web.routes.AuthenticationController.login()))

  override def loginSucceeded(request: RequestHeader)(implicit context: ExecutionContext): Future[Result] =
    Future.successful(Redirect(controllers.web.routes.WebController.index()))

  override def authorizationFailed(request: RequestHeader, user: UserModel, authority: Option[String])(implicit context: ExecutionContext): Future[Result] =
    Future.successful(Forbidden("no permission"))
}
