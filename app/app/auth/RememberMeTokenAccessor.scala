package app.auth

import jp.t2v.lab.play2.auth._
import play.api.mvc.{Cookie, RequestHeader, Result}

class RememberMeTokenAccessor(val maxAgeInSeconds: Int) extends CookieTokenAccessor {
  override def put(token: AuthenticityToken)(result: Result)(implicit request: RequestHeader): Result = {
    val remember = request.tags.get("rememberme").contains("true") || request.session.get("rememberme").contains("true")
    val maxAge = if (remember) Some(maxAgeInSeconds) else None
    val c = Cookie(cookieName, sign(token), maxAge, cookiePathOption, cookieDomainOption, cookieSecureOption, cookieHttpOnlyOption)
    result.withCookies(c)
  }
}
