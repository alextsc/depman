package app.di

import javax.inject.Inject

import app.di.ApplicationModule.MavenRepositoryProvider
import app.repositories._
import com.google.inject.{AbstractModule, Provider, Scopes}
import play.api.Configuration
import play.api.inject.ApplicationLifecycle

import scala.concurrent.{ExecutionContext, Future}

class ApplicationModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[MavenRepositoryImpl])
    bind(classOf[MavenRepository]).toProvider(classOf[MavenRepositoryProvider]).in(Scopes.SINGLETON)
    bind(classOf[UserDao]).to(classOf[UserDaoImpl])
  }
}

object ApplicationModule {

  class MavenRepositoryProvider @Inject()(val config: Configuration,
                                          val impl: MavenRepositoryImpl,
                                          val applicationLifecycle: ApplicationLifecycle,
                                          implicit val ec: ExecutionContext) extends Provider[MavenRepository] {

    override def get(): MavenRepository = {
      val indexDirPref: String = "maven.repository.index.directory"
      val cacheDirPref: String = "maven.repository.cache.directory"

      val indexDir = config.getString(indexDirPref).getOrElse(throw new RuntimeException(s"Preference '$indexDirPref' not set!"))
      val cacheDir = config.getString(cacheDirPref).getOrElse(throw new RuntimeException(s"Preference '$cacheDirPref' not set!"))
      impl.lock(indexDir, cacheDir)

      applicationLifecycle.addStopHook(() => Future(impl.unlock()))
      impl
    }
  }

}
