package app.javaenhancements

import java.sql

/** A value class around java.sql.Timestamp. */
class Timestamp(val underlying: java.sql.Timestamp) extends AnyVal

object Timestamp {
  def now(): Timestamp = new Timestamp(new sql.Timestamp(System.currentTimeMillis()))

  implicit def toSqlTimestamp(timestamp: Timestamp): java.sql.Timestamp = timestamp.underlying
}
