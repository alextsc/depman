package app

import org.apache.maven.index.ArtifactInfo

/** Formats given artifact infos for various build system dependency files */
trait ArtifactInfoFormatter {

  def formatMavenDependency(artifactInfo: ArtifactInfo): String

  def formatMavenDependency(artifacts: Seq[ArtifactInfo]): String

  def formatMavenDependency(groupId: String, artifactId: String, version: String) : String

  def formatGradleDependency(artifactInfo: ArtifactInfo): String

  def formatGradleDependency(artifacts: Seq[ArtifactInfo]): String

  def formatGradleDependency(groupId: String, artifactId: String, version: String) : String

  def formatSbtDependency(artifactInfo: ArtifactInfo): String

  def formatSbtDependency(artifacts: Seq[ArtifactInfo]): String

  def formatSbtDependency(groupId: String, artifactId: String, version: String) : String
}
