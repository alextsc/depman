package app

import scala.util.matching.Regex

case class SearchQuery(raw: String,
                       textQuery: String = "",
                       group: Option[String] = None,
                       artifact: Option[String] = None,
                       version: Option[String] = None) {

  def isEmptyQuery: Boolean = raw.trim.isEmpty
}

object SearchQuery {
  private val LimitRegex: Regex = """^([gav]):([^\s]+)$""".r

  def fromString(queryString: String): SearchQuery = {
    if (queryString.trim.isEmpty) return SearchQuery("", "", None, None, None)

    var group: Option[String] = None
    var artifact: Option[String] = None
    var version: Option[String] = None
    var queryTextTerms: Seq[String] = Seq.empty

    queryString.split(" ").foreach {
      case LimitRegex("g", text) => group = Some(text)
      case LimitRegex("a", text) => artifact = Some(text)
      case LimitRegex("v", text) => version = Some(text)
      case text: String => queryTextTerms = queryTextTerms :+ text
    }

    SearchQuery(queryString, queryTextTerms.mkString(" "), group, artifact, version)
  }
}
