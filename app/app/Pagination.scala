package app

case class Pagination[+T](allEntities: Seq[T], page: Int, entitiesPerPage: Int) {
  require(page > 0, "page has to be positive (>0)")
  val endIndex = page * entitiesPerPage
  val startIndex = endIndex - entitiesPerPage
  val totalEntityCount: Int = allEntities.size
  val displayedEntities: Seq[T] = allEntities.slice(startIndex, endIndex)
  val totalPages: Int = math.ceil(totalEntityCount / entitiesPerPage.toDouble).toInt
}