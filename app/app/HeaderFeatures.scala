package app

import app.HeaderFeatures.SearchFormMode
import app.HeaderFeatures.SearchFormMode.SearchFormMode

case class HeaderFeatures(showSearchbar: Boolean = true,
                          showTitlebar: Boolean = true,
                          autofocusSearchBar: Boolean = false,
                          searchQuery: Option[String] = None,
                          searchFormMode: SearchFormMode = SearchFormMode.FormMode,

                          /** Whether to show user controls such as login/register/logout in the navbar */
                          showUserControls: Boolean = true)

object HeaderFeatures {

  object SearchFormMode extends Enumeration {
    type SearchFormMode = Value

    /** Let the search form act as a form, submitting the request via GET to /search */
    val FormMode = Value
    /** Let the search form trigger an angular event when it is submitted to act within a single page app */
    val SinglePageMode = Value
  }

}