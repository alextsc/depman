package app.repositories

import javax.inject.Inject

import models.GaModel
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

class GavRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  private val gas = TableQuery[GaTable]

  private class GaTable(tag: Tag) extends Table[GaModel](tag, "GAS") {
    // @formatter:off
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def groupId = column[String]("GROUP_ID")
    def artifactId = column[String]("ARTIFACT_ID")
    // @formatter:on

    def * = (id.?, groupId, artifactId) <>(GaModel.tupled, GaModel.unapply)
  }

  private def findByGroupAndOrArtifactIdQuery(groupId: String, artifactId: String) =
    gas.filter(row => row.groupId === groupId || row.artifactId === artifactId)

  /** Add a new group/artifact combination to the table */
  def add(groupId: String, artifactId: String)(implicit ectx: ExecutionContext): Future[Int] = db.run {
    findByGroupAndOrArtifactIdQuery(groupId, artifactId).result.headOption.flatMap {
      case Some(existing) => DBIO.successful(existing.id.get)
      case None => (gas returning gas.map(_.id)) += GaModel(groupId = groupId, artifactId = artifactId)
    }.transactionally
  }

  /**
    * Find an existing group/artifact combination.
    *
    * @param groupId
    * @param artifactId
    * @return
    */
  def find(groupId: String, artifactId: String)(implicit ectx: ExecutionContext): Future[Option[GaModel]] =
    db.run(findByGroupAndOrArtifactIdQuery(groupId, artifactId).take(1).result.headOption)

  def deleteAll()(implicit ectx: ExecutionContext): Future[Unit] = db.run(gas.delete).map(_ => ())
}
