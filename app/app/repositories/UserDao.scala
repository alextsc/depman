package app.repositories

import models.UserModel

import scala.concurrent.{ExecutionContext, Future}

trait UserDao {


  /**
    * Saves a new user.
    *
    * The primary key field (id) is ignored when saving.
    *
    * @param user The user to save
    * @param ectx
    * @return A future with the saved user, including an assigned primary key/id
    */
  def create(user: UserModel)(implicit ectx: ExecutionContext): Future[UserModel]


  /**
    * Find a user via his id.
    *
    * @param id
    * @param ectx
    * @return A future with an option containing the user if he exists, or None if he doesn't.
    */
  def findById(id: Int)(implicit ectx: ExecutionContext): Future[Option[UserModel]]

  /**
    * Find a user via his unique e-mail.
    *
    * @param email The users email
    * @param ectx
    * @return A future with an option containing the user if he exists, or None if he doesn't.
    */
  def findByEmail(email: String)(implicit ectx: ExecutionContext): Future[Option[UserModel]]

  /**
    * Updates an existing user.
    *
    * If the given user instance doesn't contain an ID or no user with the given ID exists in the database
    * the returned future will fail.
    *
    * @param user
    * @param ectx
    * @return A future containing the updated and saved user.
    */
  def update(user: UserModel)(implicit ectx: ExecutionContext): Future[UserModel]

  /**
    * Delete _all_ users from the database.
    *
    * @param ectx
    * @return
    */
  def deleteAll()(implicit ectx: ExecutionContext): Future[Unit]

  /**
    * Searches for a user with the given email and validates the given password against the
    * database record. Returns the user if he was found and the password matched.
    *
    * @param email    The users email
    * @param password The password entered in a login form
    * @param ectx
    * @return Some with the user if he exists and the passwords match, otherwise None
    */
  def authenticate(email: String, password: String)(implicit ectx: ExecutionContext): Future[Option[UserModel]]

  /**
    * Checks whether a user with the given E-Mail already exists.
    *
    * @param email The E-Mail to check
    * @param ectx
    * @return
    */
  def isEmailInUse(email: String)(implicit ectx: ExecutionContext): Future[Boolean] = findByEmail(email).map(_.isDefined)

  /**
    * Checks whether at least one admin user is defined in the database.
    *
    * (This method is used for the setup filter, if no admin account is present all request will be redirected to
    * the application setup.)
    *
    * @return A future resolving to true if one or more admin accounts exist
    */
  def hasAdminAccounts()(implicit ectx : ExecutionContext): Future[Boolean]
}
