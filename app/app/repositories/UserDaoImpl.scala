package app.repositories

import java.sql.Timestamp
import javax.inject.Inject

import models.UserModel
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.dbio.DBIOAction
import slick.driver.JdbcProfile
import slick.lifted.CompiledFunction

import scala.concurrent.{ExecutionContext, Future}

class UserDaoImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] with UserDao {

  import driver.api._

  private def users = TableQuery[UserTable]

  private def roles = TableQuery[RolesTable]

  private class UserTable(tag: Tag) extends Table[UserModel](tag, "USERS") {
    // @formatter:off
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def email = column[String]("EMAIL")
    def passwordHash = column[String]("PASSWORD_HASH")
    def created = column[Timestamp]("CREATED")
    def lastUpdated = column[Timestamp]("LAST_UPDATED")
    def enabled = column[Boolean]("ENABLED")
    // @formatter:off

    def * = (id.?, email, passwordHash, created, lastUpdated, enabled) <> (UserModel.construct.tupled, UserModel.deconstruct)
  }

  private class RolesTable(tag : Tag) extends Table[(Int, String)](tag, "ROLES") {
    def userId = column[Int]("USER_ID")
    def role = column[String]("ROLE")

    def * = (userId, role)
  }

  private def findByIdQuery(id : Rep[Int]) = users.filter(_.id === id).take(1)
  private def findByEmailQuery(email : Rep[String]) = users.filter(_.email === email).take(1)

  private lazy val findRolesQueryCompiled = Compiled {(userId : Rep[Int]) => roles.filter(_.userId === userId)}
  private lazy val findAdminUserCountQueryCompiled = Compiled{ roles.filter(_.role === UserModel.RoleAdmin).length }

  override def create(user : UserModel)(implicit ectx : ExecutionContext) : Future[UserModel] = db.run {
    for {
      userId <- (users returning users.map(_.id)) += user
      insertedRoleCount <- roles ++= user.roles.map(userId -> _)
    } yield user.copy(id = Some(userId))
  }

  override def update(user: UserModel)(implicit ectx: ExecutionContext): Future[UserModel] = {
    if(user.id.isEmpty) Future.failed(new IllegalArgumentException("The given user contains no id"))
    val userId = user.id.get
    db.run {
      users.filter(_.id === userId).update(user).flatMap {
        case n : Int if n >= 1 =>
          DBIO.seq(
            users.filter(_.id === userId).update(user),
            roles.filter(_.userId === userId).delete,
            roles ++= user.roles.map(userId -> _)
          ).andThen(DBIO.successful(user))
        case _ =>
          DBIO.failed(new IllegalArgumentException(s"There is no user with the given id $userId"))
      }.transactionally
    }
  }

  override def findById(id : Int)(implicit ectx : ExecutionContext) : Future[Option[UserModel]] =
    findOneWithQuery(findByIdQuery(id))

  override def findByEmail(email : String)(implicit ectx : ExecutionContext) : Future[Option[UserModel]] =
    findOneWithQuery(findByEmailQuery(email))

  // TODO: Make this compatible with compiled queries (see findByIdQueryCompiled, findByEmailQueryCompiled)
  private def findOneWithQuery(query : Query[UserTable, UserModel, Seq])(implicit ectx : ExecutionContext) : Future[Option[UserModel]] =
    db.run(query.result.headOption)
      .flatMap {
        case Some(user) =>
          db.run(findRolesQueryCompiled(user.id.get).result).map (roles => Some(user.copy(roles = roles.map(_._2).toSet)))
        case None =>
          Future.successful(None)
      }

  override def deleteAll()(implicit ectx : ExecutionContext)  : Future[Unit] =
    db.run(users.delete).map(_ => ())

  override def authenticate(email: String, password: String)(implicit ectx: ExecutionContext): Future[Option[UserModel]] = {
   findByEmail(email).map {
     userOpt => userOpt.flatMap(user => if(user.hasPassword(password)) Some(user) else None)
   }
  }

  override def hasAdminAccounts()(implicit ectx : ExecutionContext): Future[Boolean] =
    db.run(findAdminUserCountQueryCompiled.result).map(_ > 0)
}
