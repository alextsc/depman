package app.repositories

import java.io.File
import java.util.Date
import javax.inject.Inject

import app.SearchQuery
import app.repositories.MavenRepository.Status
import org.apache.lucene.search.BooleanClause.Occur
import org.apache.lucene.search._
import org.apache.maven.index.context.{IndexCreator, IndexingContext}
import org.apache.maven.index.expr.{SearchTypedStringSearchExpression, SourcedSearchExpression}
import org.apache.maven.index.updater._
import org.apache.maven.index.{ArtifactInfo, _}
import org.apache.maven.wagon.Wagon
import org.apache.maven.wagon.events.{TransferEvent, TransferListener}
import org.apache.maven.wagon.observers.AbstractTransferListener
import org.codehaus.plexus.{DefaultContainerConfiguration, DefaultPlexusContainer, PlexusConstants, PlexusContainer}
import play.api.Configuration

import scala.collection.JavaConversions._
import scala.collection.{immutable, mutable}
import scala.concurrent.{ExecutionContext, Future}

class MavenRepositoryImpl @Inject()(config: Configuration) extends MavenRepository {

  import MavenRepositoryImpl._

  private val plexusContainer: PlexusContainer = {
    val config: DefaultContainerConfiguration = new DefaultContainerConfiguration()
    config.setClassPathScanning(PlexusConstants.SCANNING_INDEX)
    new DefaultPlexusContainer(config)
  }

  private val indexers = immutable.Seq[IndexCreator](
    plexusContainer.lookup(classOf[IndexCreator], "min"),
    plexusContainer.lookup(classOf[IndexCreator], "jarContent"),
    plexusContainer.lookup(classOf[IndexCreator], "maven-plugin")
  )
  private val indexer: Indexer = plexusContainer.lookup(classOf[Indexer])
  private val indexUpdater: IndexUpdater = plexusContainer.lookup(classOf[IndexUpdater])
  private val httpWagon: Wagon = plexusContainer.lookup(classOf[Wagon], "http")
  private val cachePostfix = config.getString("maven.repository.cache.postfix").get
  private val indexPostfix = config.getString("maven.repository.index.postfix").get
  private var centralContext: Option[IndexingContext] = None

  /** Initializes this repository instance.
    *
    * This starts up the repository within the given (sub)directories and puts a lock on them.
    * No other MavenRepository instances should use the given directories at the same time, only one can
    * maintain a lock at any given moment.
    */
  override def lock(indexDirectory: String, cacheDirectory: String): Unit = {
    val indexDir = new File(indexDirectory)
    val cacheDir = new File(cacheDirectory)
    val centralIndexDir = getIndexDirectoryForRepository(MavenRepositoryImpl.MavenCentral, indexDir)
    val centralCacheDir = getCacheDirectoryForRepository(MavenRepositoryImpl.MavenCentral, cacheDir)

    val repoName = RepoNameUrlMap.getOrElse("central", throw new RuntimeException("No repository URL defined"))

    this.centralContext = Some(indexer.createIndexingContext("central-context", "central",
      centralCacheDir, centralIndexDir, repoName, null, true, false, indexers))
  }

  /** Closes this repository. This releases the write lock on the directories used by this instance. */
  override def unlock(deleteFiles: Boolean = false): Unit = {
    this.centralContext.foreach(_.close(deleteFiles))
    this.centralContext = None
  }

  override def isLocked(): Boolean = this.centralContext.isDefined

  override def updateIndex()(implicit ec: ExecutionContext): Future[IndexUpdateResult] = Future {
    requireLock()
    println("Updating Index...")
    println("This might take a while on first run, so please be patient!")

    val listener: TransferListener = new AbstractTransferListener() {
      override def transferStarted(transferEvent: TransferEvent) = println("Downloading " + transferEvent.getResource.getName)

      override def transferProgress(transferEvent: TransferEvent, buffer: Array[Byte], length: Int) = ()

      override def transferCompleted(transferEvent: TransferEvent) = println(" - Done")
    }

    val resourceFetcher: ResourceFetcher = new WagonHelper.WagonFetcher(httpWagon, listener, null, null)
    val centralContextCurrentTimestamp: Date = centralContext.get.getTimestamp
    val updateRequest: IndexUpdateRequest = new IndexUpdateRequest(centralContext.get, resourceFetcher)
    val updateResult: IndexUpdateResult = indexUpdater.fetchAndUpdateIndex(updateRequest)

    updateResult match {
      case r: IndexUpdateResult if r.isFullUpdate =>
        println("Full update happened!")
      case r: IndexUpdateResult if r.getTimestamp.equals(centralContextCurrentTimestamp) =>
        println("No update needed, index is up to date!")
      case r: IndexUpdateResult =>
        println(s"Incremental update happened, change covered $centralContextCurrentTimestamp - ${r.getTimestamp} period.")
    }

    updateResult
  }

  private def requireLock(): Unit = {
    require(centralContext.isDefined, "Repository must be locked to update the index. Use lock() before running any operations.")
  }

  override def indexStatus()(implicit ec: ExecutionContext): Future[MavenRepository.Status] = {
    requireLock()
    val lastUpdatedTimestamp: Date = centralContext.get.getTimestamp
    Future.successful(MavenRepository.Status(lastUpdatedTimestamp != null, Option(lastUpdatedTimestamp)))
  }

  override def search(searchQuery: SearchQuery, resultCount: Int = 10, skip: Int = 0)(implicit ec: ExecutionContext): Future[Seq[ArtifactInfo]] = Future {
    requireLock()
    require(resultCount > 0, "resultCount must be positive")
    require(skip >= 0, "skip must be 0 or greater")
    val query = new BooleanQuery()
    val queryText = searchQuery.textQuery
    val expr: SearchTypedStringSearchExpression = new SearchTypedStringSearchExpression(queryText, SearchType.SCORED)

    query.add(createClassifierQuery, Occur.MUST_NOT)
    for (field <- Seq(MAVEN.ARTIFACT_ID, MAVEN.GROUP_ID, MAVEN.NAME)) {
      query.add(indexer.constructQuery(field, expr), Occur.SHOULD)
    }

    val req: IteratorSearchRequest = new IteratorSearchRequest(query, centralContext.get)
    val resp: IteratorSearchResponse = indexer.searchIterator(req)

    resp.iterator().iterator().slice(skip, skip + resultCount).toSeq
  }

  override def findGroupArtifacts(groupId: String)(implicit ec: ExecutionContext): Future[Seq[ArtifactInfo]] = {
    requireLock()
    ???
  }

  override def findGroupArtifactVersions(groupId: String, artifactId: String)(implicit ec: ExecutionContext): Future[Seq[ArtifactInfo]] = {
    requireLock()

    Future {
      val groupIdQ: Query = indexer.constructQuery(MAVEN.GROUP_ID, new SourcedSearchExpression(groupId))
      val artifactIdQ: Query = indexer.constructQuery(MAVEN.ARTIFACT_ID, new SourcedSearchExpression(artifactId))

      val query: BooleanQuery = new BooleanQuery()
      query.add(groupIdQ, Occur.MUST)
      query.add(artifactIdQ, Occur.MUST)
      query.add(createJarPackagingQuery, Occur.MUST)
      query.add(createClassifierQuery, Occur.MUST_NOT)

      val request: IteratorSearchRequest = new IteratorSearchRequest(query, Seq(centralContext.get))
      val response: IteratorSearchResponse = indexer.searchIterator(request)

      val result = mutable.ListBuffer[ArtifactInfo]()
      for (ai <- response) {
        result.append(ai)
      }

      result.toList.sortWith((l, r) => l.getArtifactVersion.compareTo(r.getArtifactVersion) > 0)
    }
  }

  private def createClassifierQuery: Query =
    indexer.constructQuery(MAVEN.CLASSIFIER, new SourcedSearchExpression(Field.NOT_PRESENT))

  private def createJarPackagingQuery: Query =
    indexer.constructQuery(MAVEN.PACKAGING, new SourcedSearchExpression("jar"))

  override def getGroupArtifactVersionInfo(groupId: String,
                                           artifactId: String,
                                           version: String)(implicit ec: ExecutionContext): Future[Option[ArtifactInfo]] = {
    requireLock()
    Future {

      val groupIdQ: Query = indexer.constructQuery(MAVEN.GROUP_ID, new SourcedSearchExpression(groupId))
      val artifactIdQ: Query = indexer.constructQuery(MAVEN.ARTIFACT_ID, new SourcedSearchExpression(artifactId))
      val versionQ: Query = indexer.constructQuery(MAVEN.VERSION, new SourcedSearchExpression(version))

      val query: BooleanQuery = new BooleanQuery()
      query.add(groupIdQ, Occur.MUST)
      query.add(artifactIdQ, Occur.MUST)
      query.add(versionQ, Occur.MUST)
      query.add(createJarPackagingQuery, Occur.MUST)
      query.add(createClassifierQuery, Occur.MUST_NOT)

      val req: FlatSearchRequest = new FlatSearchRequest(query, centralContext.get)
      val resp: FlatSearchResponse = indexer.searchFlat(req)
      val results = resp.getResults

      if (results.size() == 0) None else Some(results.iterator().next())
    }
  }

  override def getArtifactUrl(artifactInfo: ArtifactInfo): Option[String] = {
    if (artifactInfo.packaging != "jar") return None

    val jarName = artifactInfo.artifactId + "-" + artifactInfo.version + ".jar"
    val relativePath = "/" + artifactInfo.groupId + "/" + artifactInfo.artifactId + "/" + artifactInfo.version
    RepoNameUrlMap.get(artifactInfo.repository).map(_ + relativePath + "/" + jarName)
  }

  override def doesGroupIdExist(groupId: String)(implicit ec: ExecutionContext): Future[Boolean] = {
    requireLock()

    Future {
      val groupIdQ: Query = indexer.constructQuery(MAVEN.GROUP_ID, new SourcedSearchExpression(groupId))
      val req: FlatSearchRequest = new FlatSearchRequest(groupIdQ, centralContext.get)
      val resp: FlatSearchResponse = indexer.searchFlat(req)
      resp.getResults.size() > 0
    }
  }

  override def doesArtifactExist(groupId: String, artifactId: String)(implicit ec: ExecutionContext): Future[Boolean] = {
    requireLock()

    Future {
      val groupIdQ: Query = indexer.constructQuery(MAVEN.GROUP_ID, new SourcedSearchExpression(groupId))
      val artifactIdQ: Query = indexer.constructQuery(MAVEN.ARTIFACT_ID, new SourcedSearchExpression(artifactId))
      val query = new BooleanQuery()
      query.add(groupIdQ, Occur.MUST)
      query.add(artifactIdQ, Occur.MUST)

      val req: FlatSearchRequest = new FlatSearchRequest(query, centralContext.get)
      val resp: FlatSearchResponse = indexer.searchFlat(req)
      resp.getResults.size() > 0
    }
  }

  private def getCacheDirectoryForRepository(repoName: String, cacheDir: File): File = new File(cacheDir, repoName + "-" + cachePostfix)

  private def getIndexDirectoryForRepository(repoName: String, indexDir: File): File = new File(indexDir, repoName + "-" + indexPostfix)
}

object MavenRepositoryImpl {
  final val MavenCentral = "central"
  private val RepoNameUrlMap = Map(MavenCentral -> "http://repo1.maven.org/maven2")
}