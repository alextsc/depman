package app.repositories

import java.sql.Date
import javax.inject.Inject

import app.repositories.UpdateLogRepository.UpdateLogEntry
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

class UpdateLogRepositoryImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] with UpdateLogRepository {

  import driver.api._

  private val logEntries = TableQuery[UpdateLogEntryTable]

  class UpdateLogEntryTable(tag: Tag) extends Table[UpdateLogEntry](tag, "UPDATE_LOG") {
    // @formatter:off
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def repositoryId  = column[String]("REPOSITORY_ID")
    def started = column[Date]("STARTED")
    def ended = column[Date]("ENDED")
    def status = column[String]("STATUS")
    def message = column[String]("MESSAGE")
    // @formatter:on

    def * = (id.?, repositoryId, started, ended, status, message.?) <>(UpdateLogEntry.tupled, UpdateLogEntry.unapply)
  }

  override def newStartEntry(repositoryId: String, message: Option[String])(implicit e: ExecutionContext): Future[UpdateLogEntry] = {
    val entry: UpdateLogEntry = UpdateLogEntry(None, repositoryId, now, now, UpdateLogRepository.StatusOngoing, message)
    db.run((logEntries returning logEntries.map(_.id)) += entry).map(id => entry.copy(id = Some(id)))
  }

  override def markEntryWithError(updateLogEntry: UpdateLogEntry, message: String)(implicit e: ExecutionContext): Future[Unit] = {
    val updatedEntry = updateLogEntry.copy(status = UpdateLogRepository.StatusError, ended = now, message = Some(message))
    db.run(logEntries.filter(_.id === updateLogEntry.id.get).update(updatedEntry)).map(r => ())
  }

  override def markEntryAsFinished(updateLogEntry: UpdateLogEntry, message: Option[String])(implicit e: ExecutionContext): Future[Unit] = {
    val updatedEntry = updateLogEntry.copy(status = UpdateLogRepository.StatusFinished, ended = now, message = message)
    db.run(logEntries.filter(_.id === updateLogEntry.id.get).update(updatedEntry)).map(r => ())
  }

  override def getLogEntries(numEntries: Int, entryOffset: Int, repositoryId: Option[String])(implicit e: ExecutionContext)
  : Future[Seq[UpdateLogEntry]] = db.run(logEntries.drop(entryOffset).take(numEntries).result)

  private def now: Date = new Date(System.currentTimeMillis())
}
