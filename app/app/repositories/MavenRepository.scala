package app.repositories

import java.util.Date

import app.SearchQuery
import app.repositories.MavenRepository.{RepositoryLockedException, Status}
import org.apache.maven.index.ArtifactInfo
import org.apache.maven.index.updater.IndexUpdateResult

import scala.concurrent.{ExecutionContext, Future}

trait MavenRepository {

  def updateIndex()(implicit ec: ExecutionContext): Future[IndexUpdateResult]

  def indexStatus()(implicit ec: ExecutionContext): Future[Status]

  def search(query: SearchQuery, resultCount: Int = 10, skip: Int = 0)(implicit ec: ExecutionContext): Future[Seq[ArtifactInfo]]

  def findGroupArtifacts(groupId: String)(implicit ec: ExecutionContext): Future[Seq[ArtifactInfo]]

  def findGroupArtifactVersions(groupId: String, artifactId: String)(implicit ec: ExecutionContext): Future[Seq[ArtifactInfo]]

  def getGroupArtifactVersionInfo(groupId: String,
                                  artifactId: String,
                                  version: String)(implicit ec: ExecutionContext): Future[Option[ArtifactInfo]]

  def doesGroupIdExist(groupId: String)(implicit ec: ExecutionContext): Future[Boolean]

  def doesArtifactExist(groupId: String, artifactId: String)(implicit ec: ExecutionContext): Future[Boolean]

  def getArtifactUrl(artifactInfo: ArtifactInfo): Option[String]

  /** Initializes this repository instance and locks the given directories.
    *
    * No other MavenRepository instances should use the given directories at the same time, only one can
    * maintain a lock at any given moment.
    */
  @throws(classOf[RepositoryLockedException])
  def lock(indexDirectory: String, cacheDirectory: String): Unit

  /** Releases the write lock on the directories used by this instance. */
  def unlock(deleteFiles: Boolean = false): Unit

  def isLocked(): Boolean
}

object MavenRepository {

  class RepositoryLockedException extends Exception

  case class Status(initialized: Boolean = false, lastUpdated: Option[Date])

}

