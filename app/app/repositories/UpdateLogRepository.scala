package app.repositories

import java.sql.Date

import app.repositories.UpdateLogRepository.UpdateLogEntry

import scala.concurrent.{ExecutionContext, Future}

trait UpdateLogRepository {

  /**
    * Save a new update log entry in the database. This will be marked with status ongoing.
    *
    * @param repositoryId An identifier for the repository which is being updated
    * @param message      An optional message for the log
    * @return A future containing the newly created log entry. This should be cached
    *         and used to mark the entry as finished/errored later.
    */
  def newStartEntry(repositoryId: String, message: Option[String] = None)(implicit e: ExecutionContext): Future[UpdateLogEntry]

  /**
    * Marks the given entry with status finished.
    *
    * @param updateLogEntry The entry to mark.
    * @param message        An optional message.
    * @return A future that completes normally if the entry has been successfully updated.
    */
  def markEntryAsFinished(updateLogEntry: UpdateLogEntry, message: Option[String] = None)(implicit e: ExecutionContext): Future[Unit]

  /**
    * Marks an entry with the error status. This means the update failed.
    *
    * @param updateLogEntry The entry to update
    * @param message        A error message
    * @return A future that completes normally if the entry has been successfully updated.
    */
  def markEntryWithError(updateLogEntry: UpdateLogEntry, message: String)(implicit e: ExecutionContext): Future[Unit]

  /**
    * Get the latest log entries
    *
    * @param numEntries   The number of entries to return. The result can be smaller if not enough entries exist.
    * @param entryOffset  The offset of entries to skip before returning results. This can be used for pagination.
    * @param repositoryId An optional repository identifier. If present only entries from that repo will be returned, otherwise
    *                     entries from all repositories are fetched.
    * @return A future containing a sequence of log entries. May be empty if no entries with the given parameters exist.
    */
  def getLogEntries(numEntries: Int = 10, entryOffset: Int = 0, repositoryId: Option[String] = None)(implicit e: ExecutionContext): Future[Seq[UpdateLogEntry]]
}

object UpdateLogRepository {

  /** The update is ongoing. */
  val StatusOngoing = "ongoing"
  /** The update finished successfully. */
  val StatusFinished = "finished"
  /** The updated didn't finish successfully, an error occured. */
  val StatusError = "error"

  case class UpdateLogEntry(id: Option[Int] = None, repositoryId: String, started: Date, ended: Date, status: String, message: Option[String])

}
