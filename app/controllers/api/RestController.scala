package controllers.api

import javax.inject.Inject

import app.ArtifactInfoFormatterImpl
import app.repositories.MavenRepository
import controllers.api.results.{RestApiError, SearchResult}
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext

class RestController @Inject()(val mavenRepo: MavenRepository, implicit val ec : ExecutionContext) extends Controller {

  import RestController._

  def favoriteDependency(identifier: String, versionRange: Option[String] = None) = play.mvc.Results.TODO

  def unfavoriteDependency(identifier: String) = play.mvc.Results.TODO

  def createList(name: String) = play.mvc.Results.TODO

  def deleteList(name: String) = play.mvc.Results.TODO

  def addDependencyToList(listName: String, dependencyIdentifier: String) = play.mvc.Results.TODO

  def removeDependencyFromList(listName: String, dependencyIdentifier: String) = play.mvc.Results.TODO

  def getList(name: String, format: String) = play.mvc.Results.TODO

  def getGavDetails(groupId: String, artifactId: String, version: String) = play.mvc.Results.TODO

  def getGavDependencies(groupId: String, artifactId: String, version: String) = Action {
    val json = Json.toJson(Map(
      "gradle" -> ArtifactInfoFormatterImpl.formatGradleDependency(groupId, artifactId, version),
      "sbt" -> ArtifactInfoFormatterImpl.formatSbtDependency(groupId, artifactId, version),
      "maven" -> ArtifactInfoFormatterImpl.formatMavenDependency(groupId, artifactId, version)
    ))

    Ok(json)
  }

  def search(query: String, category: String) = Action {
    if (!ValidSearchCategories.contains(category)) {
      BadRequest(Json.toJson(RestApiError(400, s"Invalid category '$category'")))
    } else {
      category match {
        case "all" =>
          Ok(Json.toJson(tmpSearchResultsAll))
        case "groups" =>
          Ok(Json.toJson(tmpSearchResultsGroups))
        case _ =>
          NotImplemented("TODO")
      }
    }
  }
}

object RestController {
  val ValidSearchCategories = Set("all", "favorites", "groups", "artifacts")

  val tmpSearchResultsAll = Seq(
    SearchResult.group("com.foo"),
    SearchResult.artifact("com.foo", Some("config"), Some("Lightbend config for Java/Scala")),
    SearchResult.artifact("com.foo", Some("config"), Some("Lightbend config for Java/Scala"), Some("123.45"), Some(System.currentTimeMillis())),
    SearchResult.artifact("com.foo", Some("config"), Some("Lightbend config for Java/Scala"), Some("123.45"), Some(System.currentTimeMillis()), Some(true))
  )

  val tmpSearchResultsGroups = Seq(
    SearchResult.group("com.foo"),
    SearchResult.group("com.typesafe"),
    SearchResult.group("org.some.thing")
  )
}