package controllers.api

import javax.inject.Inject

import app.repositories.{MavenRepository, UserDao}
import controllers.web.Authentication
import play.api.Logger
import play.api.mvc.{Action, Controller}
import models.UserModel._

import scala.concurrent.ExecutionContext

class RestAdminController @Inject()(val userRepository: UserDao,
                                    val mavenRepo: MavenRepository,
                                    implicit val ec: ExecutionContext) extends Controller with Authentication {

  def updateRepositoryIndex() = AsyncStack(AuthorityKey -> RoleUser) { implicit request =>
    Logger.info("Updating maven repo index")
    mavenRepo.updateIndex().map(r => Ok("updated maven index"))
  }

}
