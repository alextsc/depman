package controllers.api.results

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class SearchResult(resultType: String,
                        group: String,
                        artifact: Option[String] = None,
                        description: Option[String] = None,
                        latestVersion: Option[String] = None,
                        latestVersionTimestamp: Option[Long] = None,
                        favorite: Option[Boolean] = None)

object SearchResult {

  implicit val searchResultWrites: Writes[SearchResult] = (
    (JsPath \ "type").write[String] and
      (JsPath \ "data" \ "group").write[String] and
      (JsPath \ "data" \ "artifact").writeNullable[String] and
      (JsPath \ "data" \ "description").writeNullable[String] and
      (JsPath \ "data" \ "latest-version").writeNullable[String] and
      (JsPath \ "data" \ "latest-version-timestamp").writeNullable[Long] and
      (JsPath \ "data" \ "favorite").writeNullable[Boolean]
    ) (unlift(SearchResult.unapply))

  def group(groupId: String): SearchResult = apply("group", groupId)

  def artifact(groupId: String,
               artifact: Option[String] = None,
               description: Option[String] = None,
               latestVersion: Option[String] = None,
               latestVersionTimestamp: Option[Long] = None,
               favorite: Option[Boolean] = None): SearchResult = {
    apply("artifact", groupId, artifact, description, latestVersion, latestVersionTimestamp, favorite)
  }
}