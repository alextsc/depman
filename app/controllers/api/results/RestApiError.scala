package controllers.api.results

import play.api.libs.json.Json

case class RestApiError(status: Int, message: String)

object RestApiError {
  implicit val restApiErrorFormat = Json.format[RestApiError]
}