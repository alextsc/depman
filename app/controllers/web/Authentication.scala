package controllers.web

import app.auth.AuthConfigImpl
import jp.t2v.lab.play2.auth.{AuthElement, OptionalAuthElement}
import play.api.mvc.Controller

trait Authentication extends AuthConfigImpl with AuthElement {
  this : Controller =>
}
