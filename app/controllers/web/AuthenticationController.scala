package controllers.web

import javax.inject.Inject

import app.repositories.UserDao
import controllers.web.forms.LoginForm.LoginFormData
import controllers.web.forms.RegistrationForm.RegistrationFormData
import controllers.web.forms.{LoginForm, RegistrationForm}
import jp.t2v.lab.play2.auth.LoginLogout
import models.UserModel
import play.api.Logger
import play.api.data.Form
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc.{Controller, Result}

import scala.concurrent.{ExecutionContext, Future}

class AuthenticationController @Inject()(val userRepository: UserDao,
                                         val messagesApi: MessagesApi,
                                         implicit val ec: ExecutionContext)
  extends Controller with LoginLogout with OptionalAuthentication with I18nSupport {

  def registration() = StackAction { implicit request =>
    loggedIn match {
      case Some(_) => Redirect(controllers.web.routes.WebController.index())
      case _ => Ok(views.html.registration(RegistrationForm.form))
    }
  }

  def submitRegistration() = AsyncStack { implicit request =>
    val boundForm: Form[RegistrationFormData] = RegistrationForm.form.bindFromRequest()
    boundForm.fold(
      formWithErrors => {
        Future.successful(BadRequest(views.html.registration(formWithErrors)))
      },
      formData => {
        userRepository.
          isEmailInUse(formData.email).
          flatMap {
            case true =>
              val resultForm = boundForm.withError("email", Messages("registration.email.alreadyinuse"))
              Future.successful(BadRequest(views.html.registration(resultForm)))
            case false =>
              userRepository.
                create(UserModel(formData.email, formData.password, UserModel.RolesSetUser)).
                flatMap(savedUser => gotoLoginSucceeded(savedUser.id.get)(request, ec))
          }
      }
    )
  }

  def login() = StackAction { implicit request =>
    val refererUrl = request.headers("referer")
    Logger.info(s"Login(), ref was $refererUrl")

    val user: Option[UserModel] = loggedIn
    user match {
      case Some(_) => Redirect(controllers.web.routes.WebController.index())
      case _ => Ok(views.html.login(LoginForm.form))
    }
  }

  def submitLogin() = AsyncStack { implicit request =>
    def authenticateFormData(boundForm: Form[LoginFormData]): Future[Result] = {
      val formData = boundForm.get
      userRepository.authenticate(formData.email, formData.password).flatMap {
        case Some(userModel) =>
          val req = request.copy(tags = request.tags + ("rememberme" -> formData.rememberme.toString))
          gotoLoginSucceeded(userModel.id.get)(req, ec)
        case None =>
          Future.successful {
            Redirect(controllers.web.routes.AuthenticationController.login())
              .flashing("error" -> Messages("login.login.invalidcredentials"))
          }
      }
    }

    val boundForm: Form[LoginFormData] = LoginForm.form.bindFromRequest
    boundForm.fold(
      formWithErrors =>
        Future.successful(BadRequest(views.html.login(formWithErrors))),
      formData =>
        authenticateFormData(boundForm)
    )
  }

  def logout() = AsyncStack { implicit request =>
    gotoLogoutSucceeded.map(_.flashing("success" -> Messages("login.logout.success")))
  }
}
