package controllers.web

import javax.inject.Inject

import app.repositories.{MavenRepository, UserDao}
import app.{Pagination, SearchQuery}
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Controller, Result}

import scala.concurrent.{ExecutionContext, Future}

class WebController @Inject()(val userRepository: UserDao,
                              val mavenRepository: MavenRepository,
                              val config: Configuration,
                              val messagesApi: MessagesApi,
                              implicit val ec: ExecutionContext)
  extends Controller with OptionalAuthentication with I18nSupport {

  private val SearchResultsPerPage = config.getInt("app.search.resultcount").getOrElse(10)

  def index() = StackAction { implicit request =>
    Ok(views.html.frontpage())
  }

  def search(queryString: String, pageParam: Int) = AsyncStack { implicit request =>
    val page = if (pageParam > 0) pageParam else 1
    val query: SearchQuery = SearchQuery.fromString(queryString)

    def performSearch(): Future[Result] = {
      mavenRepository.
        search(query, resultCount = SearchResultsPerPage, skip = (page - 1) * SearchResultsPerPage).
        map(result => Ok(views.html.searchresults(query, result)))
    }

    val gDefined = query.group.isDefined
    val aDefined = query.artifact.isDefined
    val vDefined = query.version.isDefined

    if (gDefined && aDefined && vDefined) {
      val g: String = query.group.get
      val a: String = query.artifact.get
      val v: String = query.version.get

      mavenRepository.
        getGroupArtifactVersionInfo(g, a, v).
        flatMap {
          case Some(_) => Future(Redirect(controllers.web.routes.WebController.groupArtifactVersionDetails(g, a, v)))
          case None => performSearch()
        }
    } else if (gDefined && aDefined && !vDefined) {
      val g: String = query.group.get
      val a: String = query.artifact.get

      mavenRepository.
        doesArtifactExist(g, a).
        flatMap {
          case true => Future(Redirect(controllers.web.routes.WebController.groupArtifactDetails(g, a)))
          case false => performSearch()
        }
    } else if (gDefined && !aDefined && !vDefined) {
      val g: String = query.group.get

      mavenRepository.doesGroupIdExist(g)
        .flatMap {
          case true => Future(Redirect(controllers.web.routes.WebController.groupDetails(g)))
          case false => performSearch()
        }
    } else if (query.isEmptyQuery) {
      Future.successful(BadRequest(views.html.searchresults(query, Seq.empty)))
    } else {
      performSearch()
    }
  }

  def groupDetails(groupId: String) = StackAction { implicit request =>
    Ok(views.html.gdetails())
  }

  def groupArtifactDetails(groupId: String, artifactId: String, page: Int) = AsyncStack { implicit request =>
    mavenRepository.findGroupArtifactVersions(groupId, artifactId).
      map(Pagination(_, if (page > 0) page else 1, 10)).
      map(pagination => Ok(views.html.gadetails(pagination)))
  }

  def groupArtifactVersionDetails(groupId: String, artifactId: String, version: String) = AsyncStack { implicit request =>
    mavenRepository.getGroupArtifactVersionInfo(groupId, artifactId, version).map {
      case Some(result) =>
        val downloadUrl = mavenRepository.getArtifactUrl(result)
        Ok(views.html.gavdetails(result, downloadUrl))
      case None =>
        NotFound(views.html.nosuchartifact())
    }
  }

  def favorites() = play.mvc.Results.TODO

  def lists() = play.mvc.Results.TODO
}