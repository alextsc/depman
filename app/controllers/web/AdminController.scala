package controllers.web

import javax.inject.Inject

import app.repositories.{MavenRepository, UserDao}
import models.UserModel
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Controller

import scala.concurrent.{ExecutionContext, Future}

class AdminController @Inject()(val userRepository: UserDao,
                                val mvnRepo: MavenRepository,
                                val messagesApi: MessagesApi,
                                implicit val ec: ExecutionContext) extends Controller with Authentication with I18nSupport {

  def panel = AsyncStack(AuthorityKey -> UserModel.RoleAdmin) { implicit request =>
    mvnRepo.indexStatus().map(status => Ok(views.html.admin_dashboard(status)))
  }
}
