package controllers.web.forms

import play.api.data.Form
import play.api.data.Forms._

object RegistrationForm {

  val form = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText(minLength = 6)
    )(RegistrationFormData.apply)(RegistrationFormData.unapply)
  )

  case class RegistrationFormData(email: String, password: String)
}
