package controllers.web.forms

import play.api.data.Form
import play.api.data.Forms._


object SetupForm {

  val form = Form(
    mapping(
      "admin-email" -> email,
      "admin-password" -> nonEmptyText
    )(SetupFormData.apply)(SetupFormData.unapply)
  )

  case class SetupFormData(adminEmail: String, adminPlaintextPassword: String)

}
