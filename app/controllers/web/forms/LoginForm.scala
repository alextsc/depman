package controllers.web.forms

import play.api.data.Form
import play.api.data.Forms._

object LoginForm {
  val form = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText,
      "rememberme" -> boolean
    )(LoginFormData.apply)(LoginFormData.unapply)
  )

  case class LoginFormData(email: String, password: String, rememberme: Boolean = false)

}
