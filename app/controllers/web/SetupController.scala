package controllers.web

import javax.inject.Inject

import app.repositories.UserDao
import controllers.web.forms.SetupForm
import models.UserModel
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller}

import scala.concurrent.{ExecutionContext, Future}

class SetupController @Inject()(val userDao: UserDao,
                                val messagesApi: MessagesApi,
                                implicit val ec: ExecutionContext) extends Controller with I18nSupport {
  def submitSetup = Action.async { implicit request =>
    val boundForm = SetupForm.form.bindFromRequest()

    boundForm.fold(
      formWithErrors => {
        Future.successful(BadRequest(views.html.setup(formWithErrors)))
      },
      formData => {
        val adminUser = UserModel(formData.adminEmail, formData.adminPlaintextPassword, UserModel.RolesSetAdmin)
        userDao.create(adminUser).map { result =>
          Redirect(routes.WebController.index())
        }
      }
    )
  }
}
