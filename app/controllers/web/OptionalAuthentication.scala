package controllers.web

import app.auth.AuthConfigImpl
import jp.t2v.lab.play2.auth.OptionalAuthElement
import play.api.mvc.Controller

trait OptionalAuthentication extends AuthConfigImpl with OptionalAuthElement {
  this : Controller =>
}
