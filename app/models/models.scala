package models

import java.sql.{Timestamp => SqlTimestamp}

case class GaModel(id : Option[Int] = None, groupId: String, artifactId: String)

case class GavFavoriteModel(id: Option[Int] = None,
                            gaId : Int,
                            version: Option[String],
                            ownerId: Int,
                            created: SqlTimestamp,
                            lastUpdated: SqlTimestamp)

object GavFavoriteModel {
  final val VersionNewest = "newest"
}

case class GavFavoriteListModel(id: Option[Int] = None,
                                ownerId: Int,
                                created: SqlTimestamp,
                                lastUpdated: SqlTimestamp)

