package models

import java.sql.{Timestamp => SqlTimestamp}

import app.javaenhancements.Timestamp
import com.github.t3hnar.bcrypt._

case class UserModel(id: Option[Int] = None,
                     email: String,
                     passwordHash: String,
                     created: SqlTimestamp,
                     lastUpdated: SqlTimestamp,
                     enabled: Boolean = true,
                     roles: Set[String] = Set.empty) {

  def hasPassword(plaintextPassword: String): Boolean = plaintextPassword.isBcrypted(this.passwordHash)

  def hasRole(role: String) : Boolean = roles.contains(role)
}

object UserModel {

  val RoleUser = "role_user"
  val RoleAdmin = "role_admin"

  val RolesSetAdmin = Set(RoleUser, RoleAdmin)
  val RolesSetUser = Set(RoleUser)

  val construct = apply(_: Option[Int], _: String, _: String, _: SqlTimestamp, _: SqlTimestamp, _: Boolean, Set.empty)

  def deconstruct(u: UserModel) = Some(u.id, u.email, u.passwordHash, u.created, u.lastUpdated, u.enabled)

  def apply(email: String, plaintextPassword: String): UserModel = apply(email, plaintextPassword, Set.empty)

  def apply(email: String, plaintextPassword: String, roles: Set[String]): UserModel =
    apply(None, email, plaintextPassword.bcrypt, Timestamp.now(), Timestamp.now(), enabled = true, roles)
}