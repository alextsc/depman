package filters

import javax.inject.Inject

import akka.stream.Materializer
import app.repositories.UserDao
import controllers.web.forms.SetupForm
import play.api.Logger
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Results._
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Checks whether all criteria for a correctly setup application are met. If that is not the case
  * all request are redirected to the setup page.
  *
  * Current criteria:
  * - Is there at least one admin user present
  */
class SetupCompleteFilter @Inject()(val userDao: UserDao,
                                    val mat: Materializer,
                                    val messagesApi: MessagesApi,
                                    implicit val ec: ExecutionContext) extends Filter with I18nSupport {

  import SetupCompleteFilter._

  override def apply(nextFilter: (RequestHeader) => Future[Result])(reqHeader: RequestHeader): Future[Result] = {
    val isGetRequest: Boolean = reqHeader.method == "GET"
    val inIgnoredPaths: Boolean = IgnoredPaths.exists(reqHeader.path.startsWith(_))

    if (inIgnoredPaths || !isGetRequest) {
      nextFilter(reqHeader)
    } else {
      userDao.hasAdminAccounts().flatMap({
        case true =>
          nextFilter(reqHeader)
        case false =>
          Future.successful(Ok(views.html.setup(SetupForm.form)))
      })
    }
  }
}

object SetupCompleteFilter {
  /** All paths starting with these segments are ignored by this filter */
  val IgnoredPaths = Seq("/rest", "/webjars", "/assets", "/setup")
}
