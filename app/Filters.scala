import javax.inject.Inject

import filters.SetupCompleteFilter
import play.api.http.HttpFilters
import play.api.mvc.EssentialFilter

class Filters @Inject()(val setupFilter: SetupCompleteFilter) extends HttpFilters {
  override def filters: Seq[EssentialFilter] = Seq(setupFilter)
}
