package testing

import play.api.Application
import play.api.libs.ws.WSClient

trait WsSpec {
  val app: Application
  val port: Int

  val wsClient = app.injector.instanceOf[WSClient]
  val localAddress = s"localhost:$port"
}
