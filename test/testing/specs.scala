package testing

import org.openqa.selenium.WebDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.scalatest._
import org.scalatestplus.play._
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder

/** Spec for basic unit tests */
trait UnitSpec extends FunSpec with Matchers

/** Spec for integration tests that don't require a running play instance (maven service testing,...) */
trait IntegrationSpec extends UnitSpec

/** Spec for integration tests which require a running play server */
trait PlayServerSpec extends IntegrationSpec with OneServerPerSuite {

  implicit override lazy val app: Application = new GuiceApplicationBuilder().
    configure(
      "slick.dbs.default.driver" -> "slick.driver.H2Driver$",
      "slick.dbs.default.db.driver " -> "org.h2.Driver",
      "slick.dbs.default.db.url " -> "jdbc:h2:mem:testdb",
      "slick.dbs.default.db.user " -> "sa",
      "slick.dbs.default.db.password " -> "",
      "evolutionplugin" -> "enabled",
      "play.evolutions.db.default.autoApply" -> true,
      "play.evolutions.db.default.autoApplyDowns" -> true
    ).
    build()
}

/** Spec for integration tests which require a running play server and browser */
trait PlayServerBrowserSpec extends IntegrationSpec with OneServerPerSuite with OneBrowserPerTest {
  override def createWebDriver(): WebDriver = {
    new HtmlUnitDriver(true)
  }
}

object TestTags {

  object UnitTest extends Tag("de.alextsc.test.UnitTest")

  object IntegrationTest extends Tag("de.alextsc.test.IntegrationTest")

}