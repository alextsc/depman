package app

import app.SearchQuery
import testing.UnitSpec

class SearchQuerySpec extends UnitSpec {
  describe("SearchQuery.fromString()") {
    it("should process a simple query") {
      val searchQuery: SearchQuery = SearchQuery.fromString("junit")
      searchQuery.textQuery should include("junit")
      searchQuery.group should be(None)
      searchQuery.artifact should be(None)
      searchQuery.version should be(None)
    }

    it("should process a group limit only query") {
      val searchQuery: SearchQuery = SearchQuery.fromString("g:junit")
      searchQuery.textQuery should be("")
      searchQuery.group should be(Some("junit"))
      searchQuery.artifact should be(None)
      searchQuery.version should be(None)
    }

    it("should process a artifact limit only query") {
      val searchQuery: SearchQuery = SearchQuery.fromString("a:junit")
      searchQuery.textQuery should be("")
      searchQuery.group should be(None)
      searchQuery.artifact should be(Some("junit"))
      searchQuery.version should be(None)
    }

    it("should process a version limit only query") {
      val searchQuery: SearchQuery = SearchQuery.fromString("v:12.0")
      searchQuery.textQuery should be("")
      searchQuery.group should be(None)
      searchQuery.artifact should be(None)
      searchQuery.version should be(Some("12.0"))
    }

    it("should process complex query") {
      val searchQuery: SearchQuery = SearchQuery.fromString("g:com.typesafe logging v:3.0.0")
      searchQuery.textQuery should include("logging")
      searchQuery.group should be(Some("com.typesafe"))
      searchQuery.artifact should be(None)
      searchQuery.version should be(Some("3.0.0"))
    }
  }
}
