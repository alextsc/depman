package app

import org.apache.maven.index.ArtifactInfo
import org.scalatest.mock.MockitoSugar
import testing.UnitSpec

class ArtifactFormatterImplSpec extends UnitSpec with MockitoSugar {

  final val ExampleGroupId = "com.ex"
  final val ExampleArtifactId = "example"
  final val ExampleVersion = "42.0.1"

  describe("formatSbtDependency") {
    it("should format a simple dependency") {
      val artifactInfo = mockArtifactInfo()
      val result = ArtifactInfoFormatterImpl.formatSbtDependency(artifactInfo)
      result should be("""libraryDependencies += "com.ex" % "example" % "42.0.1"""")
    }
  }

  describe("formatGradleDependency()") {
    it("should format a single dependency") {
      val artifactInfo = mockArtifactInfo()
      val result = ArtifactInfoFormatterImpl.formatGradleDependency(artifactInfo)
      result should be("""com.ex:example:42.0.1""")
    }
  }

  private def mockArtifactInfo(groupId: String = ExampleGroupId,
                               artifactId: String = ExampleArtifactId,
                               version: String = ExampleVersion): ArtifactInfo = {
    val artifactInfo = mock[ArtifactInfo]
    artifactInfo.groupId = groupId
    artifactInfo.artifactId = artifactId
    artifactInfo.version = version
    artifactInfo
  }
}
