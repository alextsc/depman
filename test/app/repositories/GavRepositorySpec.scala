package app.repositories

import javax.inject.Inject

import app.repositories.GavRepository
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import testing.PlayServerSpec
import play.api.libs.concurrent.Execution.Implicits._

class GavRepositorySpec extends PlayServerSpec with BeforeAndAfterEach {

  private val repo = app.injector.instanceOf[GavRepository]

  override protected def afterEach(): Unit = {
    super.afterEach()
    ScalaFutures.whenReady(repo.deleteAll())(_ => ())
  }

  describe("add()") {
    it("should add any entry only once to the database") {
      ScalaFutures.whenReady(repo.add("com.example", "artifact")) {
        initialId => ScalaFutures.whenReady(repo.add("com.example", "artifact")) {
          resultId => resultId should be(initialId)
        }
      }
    }
  }

  describe("find()") {

    it("should return None if no entry exists in the database") {
      ScalaFutures.whenReady(repo.find("com.example", "artifact")) {
        result => result should be(None)
      }
    }

    it("should find an existing entry") {
      ScalaFutures.whenReady(repo.add("com.example", "artifact")) {
        r => ScalaFutures.whenReady(repo.find("com.example", "artifact")) {
          case Some(result) =>
            result.groupId should be("com.example")
            result.artifactId should be("artifact")
          case _ => fail("Expected Some(result)")
        }
      }
    }
  }
}
