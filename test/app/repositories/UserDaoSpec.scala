package app.repositories

import javax.inject.Inject

import app.repositories.UserDaoImpl
import models.UserModel
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import testing.PlayServerSpec

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._

class UserDaoSpec extends PlayServerSpec with BeforeAndAfterEach {

  private val repo = this.app.injector.instanceOf[UserDaoImpl]

  override protected def afterEach(): Unit = {
    super.afterEach()
    ScalaFutures.whenReady(repo.deleteAll())(_ => ())
  }

  describe("create()") {
    it("should insert a user") {
      val user = UserModel("joe@doe.net", "password123")
      ScalaFutures.whenReady(repo.create(user)) {
        result => result should not be (-1)
      }
    }
  }

  describe("update()") {
    it("should update a user") {
      val user = UserModel("joe@doe.net", "password123", Set("role1", "roletwo"))
      ScalaFutures.whenReady(repo.create(user).flatMap(r => repo.findById(r.id.get))) { savedUser =>
        val updatedUser = savedUser.get.copy(email = "someone@else.net", roles = Set("newrole"))
        ScalaFutures.whenReady(repo.update(updatedUser).flatMap(r => repo.findById(r.id.get))) { result =>
          val u = result.get
          u.email should be("someone@else.net")
          u.id should be(savedUser.get.id)
          u.passwordHash should be(savedUser.get.passwordHash)
          u.roles should contain("newrole")
          u.roles should have size 1
        }
      }
    }

    it("should fail correctly if no user with the given id exists") {
      val user = UserModel("joe@doe.net", "password123", Set("role1", "roletwo")).copy(id = Some(123))
      ScalaFutures.whenReady(repo.update(user).failed) { result =>
        result shouldBe a[IllegalArgumentException]
      }
    }
  }

  describe("findById()") {
    it("should find a user") {
      val user = UserModel("joe@doe.net", "password123", Set("role1", "roletwo"))
      ScalaFutures.whenReady(repo.create(user).flatMap(r => repo.findById(r.id.get))) {
        case Some(loadedUser) =>
          loadedUser.id should not be None
          loadedUser.email should be("joe@doe.net")
          loadedUser.hasPassword("password123") should be(true)
          loadedUser.roles should contain("role1")
          loadedUser.roles should contain("roletwo")
          loadedUser.roles should have size 2
        case _ => fail("Expected Some(user)")
      }
    }

    it("should return None if no user with the given ID exists") {
      ScalaFutures.whenReady(repo.findById(123)) {
        result => result should be(None)
      }
    }
  }

  describe("findByEmail()") {
    it("should find a user") {
      val user = UserModel("joe@doe.net", "password123", Set("role1", "roletwo"))
      ScalaFutures.whenReady(repo.create(user).flatMap(_ => repo.findByEmail("joe@doe.net"))) {
        case Some(loadedUser) =>
          loadedUser.id should not be None
          loadedUser.email should be("joe@doe.net")
          loadedUser.hasPassword("password123") should be(true)
          loadedUser.roles should contain("role1")
          loadedUser.roles should contain("roletwo")
          loadedUser.roles should have size 2
        case _ => fail("Expected Some(user)")
      }
    }

    it("should return None if no user with the given email exists") {
      ScalaFutures.whenReady(repo.findByEmail("x@y.net")) {
        result => result should be(None)
      }
    }
  }

  describe("hasAdminAccounts()") {
    it("should return false if no admin user exists") {
      ScalaFutures.whenReady(repo.hasAdminAccounts()) { result =>
        result should be(false)
      }
    }

    it("shoulld return true if one admin user exists") {
      val adminUser: UserModel = UserModel("admin@app.net", "asdjh", Set(UserModel.RoleAdmin))
      Await.result(repo.create(adminUser), 5 seconds)

      ScalaFutures.whenReady(repo.hasAdminAccounts()) { result =>
        result should be(true)
      }
    }
  }
}
