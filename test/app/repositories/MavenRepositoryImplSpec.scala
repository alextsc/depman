package app.repositories

import javax.inject.Inject

import app.SearchQuery
import app.repositories.MavenRepositoryImpl
import org.apache.maven.index.ArtifactInfo
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mock.MockitoSugar
import testing.PlayServerSpec

import play.api.libs.concurrent.Execution.Implicits._

class MavenRepositoryImplSpec extends PlayServerSpec with BeforeAndAfterAll with MockitoSugar {

  var repo: MavenRepositoryImpl = app.injector.instanceOf[MavenRepositoryImpl]

  describe("findArtifactVersions()") {
    ignore("should only return artifact infos for the searched artifactid/groupid") {
      ScalaFutures.whenReady(repo.findGroupArtifactVersions("junit", "junit")) {
        result => result.size should not be 0

          for (ai: ArtifactInfo <- result) {
            ai.groupId should be("junit")
            ai.artifactId should be("junit")
          }
      }
    }
  }

  describe("getArtifactUrl()") {
    it("should return a correct url with valid input") {
      val artifactInfo = mock[ArtifactInfo]
      artifactInfo.groupId = "junit"
      artifactInfo.artifactId = "junit"
      artifactInfo.version = "4.12"
      artifactInfo.repository = "central"
      artifactInfo.packaging = "jar"

      repo.getArtifactUrl(artifactInfo) should be(Some("http://repo1.maven.org/maven2/junit/junit/4.12/junit-4.12.jar"))
    }

    it("should return none for non-jar packaging") {
      val artifactInfo = mock[ArtifactInfo]
      artifactInfo.groupId = "junit"
      artifactInfo.artifactId = "junit"
      artifactInfo.version = "4.12"
      artifactInfo.repository = "central"
      // artifactInfo.packaging = "jar"

      repo.getArtifactUrl(artifactInfo) should be(None)
    }

    it("should be none if the maven repository  isnt known to the service") {
      val artifactInfo = mock[ArtifactInfo]
      artifactInfo.groupId = "junit"
      artifactInfo.artifactId = "junit"
      artifactInfo.version = "4.12"
      //      artifactInfo.repository = "central"
      artifactInfo.packaging = "jar"

      repo.getArtifactUrl(artifactInfo) should be(None)
    }
  }

  describe("search()") {
    ignore("should return results") {
      ScalaFutures.whenReady(repo.search(SearchQuery.fromString("junit"))) {
        result =>
          result.size should be > 0
      }
    }

  }
}
