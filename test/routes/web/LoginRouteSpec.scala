package routes.web

import app.repositories.UserDao
import models.UserModel
import org.scalatest.BeforeAndAfterAll
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.concurrent.Execution.Implicits._
import testing.PlayServerBrowserSpec
import testing.TestTags.IntegrationTest

import scala.concurrent.Await
import scala.concurrent.duration._

class LoginRouteSpec extends PlayServerBrowserSpec with BeforeAndAfterAll with I18nSupport {

  implicit val messagesApi = app.injector.instanceOf[MessagesApi]

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    val userDao = app.injector.instanceOf[UserDao]
    Await.result(userDao.create(UserModel("test@test.net", "testpass123", UserModel.RolesSetUser)), 5 seconds)
    Await.result(userDao.create(UserModel("admin@site.net", "testpass123", UserModel.RolesSetAdmin)), 5 seconds)
  }

  describe("/login") {
    it("should have a title containing login", IntegrationTest) {
      go to s"http://localhost:$port/login"
      pageTitle should include(Messages("login"))
    }

    it("should display a message when the entered credentials are invalid", IntegrationTest) {
      go to s"http://localhost:$port/login"
      click on "email"
      emailField("email").value = "doesnot@exist.org"
      click on "password"
      pwdField("password").value = "randompassword123"
      submit()

      eventually {
        find(tagName("body")).foreach(elem => elem.text should include(Messages("login.login.invalidcredentials")))
        emailField("email").value should be("")
        pwdField("password").value should be("")
      }
    }

    it("should allow the user to login with valid credentials", IntegrationTest) {
      go to s"http://localhost:$port/login"
      click on "email"
      emailField("email").value = "test@test.net"
      click on "password"
      pwdField("password").value = "testpass123"
      submit()

      eventually {
        find(tagName("body")).foreach(elem => elem.text should include("test@test.net"))
      }
    }
  }


}
