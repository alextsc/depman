package routes.rest

import play.api.libs.json.{JsValue, Json}
import play.api.test.{DefaultAwaitTimeout, FutureAwaits}
import testing.{PlayServerSpec, WsSpec}

class SearchRouteSpec extends PlayServerSpec with WsSpec with FutureAwaits with DefaultAwaitTimeout {
  describe("rest/1//search") {
    it("should return a proper error when receiving an invalid category") {
      val requestFuture = wsClient.url(s"http://$localAddress/rest/1/search").
        withQueryString("query" -> "foo", "category" -> "invalid").
        get()

      val response = await(requestFuture)
      response.status should be(400)

      val json: JsValue = Json.parse(response.body)
      (json \ "status").as[Int] should be(400)
      (json \ "message").asOpt[String] shouldBe 'defined
    }
  }
}
