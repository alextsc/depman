var app = angular.module("depman", ['validation.match']);

app.controller("MainController", function ($scope, $log) {
    $scope.submitSinglePageSearch = function () {
        $scope.$broadcast('search-submit');
    }
});


app.controller("SearchController", function ($scope, $http, $log) {
    $scope.categories = {
        all: "all",
        artifacts: "artifacts",
        groups: "groups",
        favorites: "favorites"
    };

    var searchUrl = "/rest/1/search";

    $scope.category = "all";
    $scope.results = [];
    $scope.searchInProgess = false;

    $scope.$on('search-submit', function (event) {
        $scope.performSearch()
    });

    $scope.init = function () {
        $scope.performSearch();
    };

    $scope.changeCategory = function (newCat) {
        if ($scope.category === newCat) return;

        $scope.category = newCat;
        $scope.performSearch();
    };

    $scope.performSearch = function () {
        if ($scope.query.trim().length === 0) return;

        $scope.searchInProgess = true;
        $http({
            method: "GET",
            url: "/rest/1/search",
            params: {
                category: $scope.category,
                query: $scope.query
            }
        }).then(function success(response) {
                $scope.results = response.data;
                $scope.searchInProgess = false;
            }, function failure(response) {
                $scope.searchInProgess = false;
                $log.error("Failed search response: " + JSON.stringify(response))
            }
        );
    };

    $scope.displayLatestVersionInResult = function (result) {
        return result.type === 'artifact' && result.data['latest-version'] && result.data['latest-version'].length > 0
    };

    $scope.init();
});